BACKEND_CONTAINER_NAME=django

build:
	docker-compose up --build

upgrade_deps:
	docker-compose run --rm $(BACKEND_CONTAINER_NAME) pip-compile --upgrade --resolver=backtracking

manage:
	docker-compose run --rm $(BACKEND_CONTAINER_NAME) manage $(filter-out $@,$(MAKECMDGOALS))

lint:
	docker-compose run --rm $(BACKEND_CONTAINER_NAME) lint

format:
	docker-compose run --rm $(BACKEND_CONTAINER_NAME) fmt

test:
	docker-compose run --rm $(BACKEND_CONTAINER_NAME) test

%: #Ignore unknown commands (and extra parameters)
	@:
