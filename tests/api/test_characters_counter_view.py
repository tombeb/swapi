from django.urls import reverse
from rest_framework.test import APIClient

import pytest

from tests.factories import CharacterFactory, PlanetFactory


@pytest.mark.django_db
def test_characters_counter_no_group_by(client):

    client = APIClient()
    url = reverse("character_counter")
    response = client.get(url)

    assert response.status_code == 400
    assert response.data["results"] is not None
    assert isinstance(response.data["results"], str)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "group_by, expected_response",
    (
        pytest.param(
            "name,hair_color,homeworld",
            [
                {
                    "count": 2,
                    "homeworld": "Planet_1",
                    "hair_color": "white",
                    "name": "Test",
                },
                {
                    "count": 1,
                    "homeworld": "Planet_2",
                    "hair_color": "black",
                    "name": "Test",
                },
            ],
        ),
        pytest.param(
            "name,homeworld",
            [
                {
                    "count": 1,
                    "homeworld": "Planet_2",
                    "name": "Test",
                },
                {
                    "count": 2,
                    "homeworld": "Planet_1",
                    "name": "Test",
                },
            ],
        ),
        pytest.param(
            "name,hair_color",
            [
                {
                    "count": 2,
                    "hair_color": "white",
                    "name": "Test",
                },
                {
                    "count": 1,
                    "hair_color": "black",
                    "name": "Test",
                },
            ],
        ),
        pytest.param(
            "hair_color,homeworld",
            [
                {
                    "count": 2,
                    "homeworld": "Planet_1",
                    "hair_color": "white",
                },
                {
                    "count": 1,
                    "homeworld": "Planet_2",
                    "hair_color": "black",
                },
            ],
        ),
    ),
)
def test_characters_counter_valid_group_by(client, group_by, expected_response):
    planet_1 = PlanetFactory(name="Planet_1")
    planet_2 = PlanetFactory(name="Planet_2")
    CharacterFactory.create(name="Test", hair_color="white", homeworld=planet_1)
    CharacterFactory.create(name="Test", hair_color="white", homeworld=planet_1)
    CharacterFactory.create(name="Test", hair_color="black", homeworld=planet_2)

    client = APIClient()

    url = f"{reverse('character_counter')}?group_by={group_by}"
    response = client.get(url)

    assert response.status_code == 200
    assert response.data["results"] == expected_response
