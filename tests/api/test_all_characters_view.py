from django.urls import reverse
from rest_framework.test import APIClient

import pytest

from tests.factories import CharacterFactory, PlanetFactory


@pytest.mark.django_db
def test_all_characters(client):
    planet_1 = PlanetFactory(name="Planet_1")
    CharacterFactory.create_batch(22, homeworld=planet_1)
    client = APIClient()
    url = reverse("all_characters")
    response = client.get(url)

    assert response.status_code == 200
    assert response.data["count"] == 22
    assert response.data["next"] is not None
    assert response.data["results"][0]["homeworld"] == "Planet_1"
