from planet.utils.parser import planet_parser
from tests.data_examples.planet import SW_API_DATA, SW_API_DATA_PARSED


def test_planet_parser():
    parsed_daa = planet_parser.parse(SW_API_DATA)

    assert parsed_daa == SW_API_DATA_PARSED
