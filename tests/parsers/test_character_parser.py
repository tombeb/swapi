from character.utils.parser import character_parser
from tests.data_examples.character import SW_API_DATA, SW_API_DATA_PARSED


def test_character_parser():
    parsed_daa = character_parser.parse(SW_API_DATA)

    assert parsed_daa == SW_API_DATA_PARSED
