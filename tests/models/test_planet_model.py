from copy import deepcopy
from unittest.mock import patch

import pytest

from planet.models import Planet
from tests.data_examples.planet import SW_API_DATA


@pytest.mark.django_db
@patch("common.collector.SWAPIDtataCollector.get")
def test_character_model(planets_data):
    planets_data.return_value = deepcopy(SW_API_DATA)
    Planet().import_data_from_api()

    assert Planet.objects.all().count() == 2
