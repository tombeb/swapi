from copy import deepcopy
from unittest.mock import patch

import pytest

from character.models import Character
from tests.data_examples.character import SW_API_DATA
from tests.factories import PlanetFactory


@pytest.mark.django_db
@patch("common.collector.SWAPIDtataCollector.get")
def test_character_model(characters_data):
    characters_data.return_value = deepcopy(SW_API_DATA)
    PlanetFactory(id=1, name="Planet_1")
    Character().import_data_from_api()

    assert Character.objects.all().count() == 2
