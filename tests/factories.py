import factory

from character.models import Character
from planet.models import Planet


class PlanetFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Planet

    name = "Tatooine"


class CharacterFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Character

    name = "Luke Skywalker"
    height = 172
    mass = 77
    hair_color = "blond"
    skin_color = "fair"
    eye_color = "blue"
    birth_year = "19BBY"
    gender = "male"
    date = "2014-12-20"
