from character.models import Character
from planet.models import Planet


class SWAPIDataUpdater:
    """
    It should run as cron task and update data in the background
    """

    def update_data_from_api(self):
        Planet().import_data_from_api()
        Character().import_data_from_api()


swapi_data_updater = SWAPIDataUpdater()
