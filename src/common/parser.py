from abc import ABC, abstractmethod


class SWAPIParser(ABC):
    @abstractmethod
    def parse(self, data: list) -> list:
        pass
