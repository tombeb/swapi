import requests


class SWAPIDtataCollector:
    def get(self, keyword) -> list:
        url: str = f"https://swapi.dev/api/{keyword}/?page=1"
        data = []
        while True:

            try:
                res: dict = requests.get(url).json()
                data += res["results"]
            except Exception:
                break

            if not res["next"]:
                break
            url = res["next"]
        return data


swapi_data_collecor = SWAPIDtataCollector()
