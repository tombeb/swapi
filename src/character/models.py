from datetime import datetime

from django.conf import settings
from django.db.models import (
    CASCADE,
    CharField,
    DateField,
    DateTimeField,
    FileField,
    FloatField,
    ForeignKey,
    Model,
    PositiveSmallIntegerField,
    TextChoices,
)

import pandas as pd

from character.utils.parser import character_parser
from common.collector import swapi_data_collecor
from planet.models import Planet


class Character(Model):
    class Genders(TextChoices):
        MALE = "MALE", "male"
        FEMALE = "FEMALE", "female"
        NONE = "n/a", "n/a"

    name = CharField(max_length=100)
    height = PositiveSmallIntegerField(null=True)
    mass = FloatField(null=True)
    hair_color = CharField(max_length=100, null=True)
    skin_color = CharField(max_length=100, null=True)
    eye_color = CharField(max_length=100, null=True)
    birth_year = CharField(max_length=100, null=True)
    gender = CharField(max_length=100, choices=Genders.choices, null=True, blank=True)
    homeworld = ForeignKey(Planet, on_delete=CASCADE)
    date = DateField()

    def __str__(self) -> str:
        return self.name

    def import_data_from_api(self):
        parsed_data = character_parser.parse(swapi_data_collecor.get("people"))
        for character in parsed_data:
            try:
                planet = Planet.objects.get(id=character.homeworld_id)
                data = character.__dict__
                data.pop("homeworld_id")
                Character.objects.update_or_create(
                    name=character.name, homeworld=planet, defaults=data
                )
            except Planet.DoesNotExist:
                continue


class CharactersFile(Model):
    csv_file = FileField(upload_to="csv/")
    created = DateTimeField(auto_now_add=True)
    name = CharField(max_length=100)

    def generate(self):
        df = pd.DataFrame.from_records(Character.objects.all().values()).sort_values(
            by="id"
        )
        filename = f"sw_characters_{datetime.now()}.csv"
        df.to_csv(f"{settings.MEDIA_ROOT}/{filename}", index=False)
        CharactersFile(name=filename, csv_file=filename).save()
