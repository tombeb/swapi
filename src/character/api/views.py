from django.db.models import Count
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from character.api.serializers import CharacterFileSerializer, CharacterSerializer
from character.models import Character, CharactersFile


class CharactersView(ListAPIView):
    serializer_class = CharacterSerializer
    queryset = Character.objects.select_related("homeworld").order_by("id")


class CharacterCounterView(ListAPIView):
    def get_queryset(self):
        fields = self.request.query_params["group_by"].split(",")
        return (
            Character.objects.select_related("homeworld")
            .values(*fields)
            .annotate(count=Count("id"))
        )

    def list(self, request, *args, **kwargs):
        try:
            return Response({"results": list(self.get_queryset())}, status=HTTP_200_OK)
        except MultiValueDictKeyError:
            return Response(
                {"results": "No group_by parameter specified!"},
                status=HTTP_400_BAD_REQUEST,
            )


class FilesListView(ListAPIView):
    queryset = CharactersFile.objects.all().order_by("-id")
    serializer_class = CharacterFileSerializer


class CharactersDownloadView(APIView):
    def get(self, request):
        CharactersFile().generate()
        csv_file = CharactersFile.objects.latest("id").csv_file.file
        response = HttpResponse(csv_file, content_type="text/csv")
        response["Content-Disposition"] = f"attachment; filename={csv_file}"
        return response
