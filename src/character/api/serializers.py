from rest_framework.serializers import ModelSerializer, SerializerMethodField

from character.models import Character, CharactersFile


class CharacterSerializer(ModelSerializer):
    class Meta:
        model = Character
        fields = (
            "name",
            "height",
            "mass",
            "hair_color",
            "skin_color",
            "eye_color",
            "birth_year",
            "gender",
            "homeworld",
            "date",
        )


class CharacterFileSerializer(ModelSerializer):
    class Meta:
        model = CharactersFile
        fields = ("name", "url")

    url = SerializerMethodField()

    def get_url(self, csv: CharactersFile) -> str:
        request = self.context.get("request")
        return request.build_absolute_uri(csv.csv_file.url)
