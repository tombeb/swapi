from django.urls import path

from character.api.views import (
    CharacterCounterView,
    CharactersDownloadView,
    CharactersView,
    FilesListView,
)


urlpatterns = [
    path("", CharactersView.as_view(), name="all_characters"),
    path("counter/", CharacterCounterView.as_view(), name="character_counter"),
    path("download/", CharactersDownloadView.as_view(), name="characters_downloader"),
    path("files/", FilesListView.as_view(), name="character_files"),
]
