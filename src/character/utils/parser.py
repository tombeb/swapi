from dataclasses import dataclass
from datetime import date

from dateutil import parser

from common.parser import SWAPIParser


@dataclass
class Character:
    name: str
    height: int | None
    mass: float | None
    hair_color: str | None
    skin_color: str | None
    eye_color: str | None
    birth_year: str | None
    gender: str | None
    homeworld_id: int
    date: date


class CharacterParser(SWAPIParser):
    def parse(self, data) -> list:
        return [
            Character(
                name=character["name"],
                height=int(character["height"])
                if not character["height"] in ["unknown", "n/a"]
                else None,
                mass=float(character["mass"].replace(",", "."))
                if not character["mass"] in ["unknown", "n/a"]
                else None,
                hair_color=character["hair_color"]
                if not character["hair_color"] in ["unknown", "n/a"]
                else None,
                skin_color=character["skin_color"]
                if not character["skin_color"] in ["unknown", "n/a"]
                else None,
                eye_color=character["eye_color"]
                if not character["eye_color"] in ["unknown", "n/a"]
                else None,
                birth_year=character["birth_year"]
                if not character["birth_year"] in ["unknown", "n/a"]
                else None,
                gender=character["gender"]
                if not character["gender"] in ["unknown", "n/a"]
                else None,
                homeworld_id=int(character["homeworld"].split("/")[-2]),
                date=parser.parse(character["edited"]),
            )
            for character in data
        ]


character_parser = CharacterParser()
