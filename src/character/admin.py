from django.contrib import admin

from character.models import Character, CharactersFile


@admin.register(Character)
class CharacterAdmin(admin.ModelAdmin):
    list_display = ("name", "homeworld", "date")
    ordering = ("name",)
    search_fields = ("^name", "^homeworld")


@admin.register(CharactersFile)
class CharactersFileAdmin(admin.ModelAdmin):
    list_display = ("name",)
