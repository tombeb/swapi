from django.db.models import CharField, Model, PositiveSmallIntegerField

from common.collector import swapi_data_collecor
from planet.utils.parser import planet_parser


class Planet(Model):

    id = PositiveSmallIntegerField(default=0)
    name = CharField(max_length=100, unique=True, primary_key=True)

    def __str__(self) -> str:
        return self.name

    def import_data_from_api(self):
        parsed_data = planet_parser.parse(swapi_data_collecor.get("planets"))
        for planet in parsed_data:
            Planet.objects.update_or_create(name=planet.name, defaults=planet.__dict__)
