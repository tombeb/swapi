from dataclasses import dataclass

from common.parser import SWAPIParser


@dataclass
class Planet:
    id: int
    name: str


class PlanetParser(SWAPIParser):
    def parse(self, data: list) -> list[Planet]:
        return [
            Planet(name=planet["name"], id=int(planet["url"].split("/")[-2]))
            for planet in data
        ]


planet_parser = PlanetParser()
