from django.core.management import call_command
from django.core.management.base import BaseCommand

from common.updater import swapi_data_updater


class Command(BaseCommand):
    def handle(self, *args, **options):
        call_command("collectstatic", interactive=False)
        call_command("migrate", interactive=False)
        swapi_data_updater.update_data_from_api()
