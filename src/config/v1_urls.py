from django.conf import settings
from django.urls import include, path
from rest_framework.permissions import AllowAny

from drf_yasg.openapi import Info
from drf_yasg.views import get_schema_view


schema_view = get_schema_view(
    Info(
        title="TB SWAPI",
        default_version="v1",
    ),
    public=True,
    permission_classes=(AllowAny,),
)

urlpatterns = [
    path("character/", include("character.api.urls")),
]

if settings.DEBUG:
    urlpatterns += [
        path(
            "docs/",
            schema_view.with_ui("swagger", cache_timeout=0),
            name="schema-swagger-ui",
        ),
    ]
